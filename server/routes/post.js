const router = require('express').Router();
const Post = require('../models/post.model');
const User = require('../models/user.model')
const LikePost = require('../models/likedpost.model');

router.post('/add', (req, res) => {

    // console.log(req.body)

    // const { username, password } = req.body
    const post = {
        body: req.body.post,
        comments: [],
        image: '',
        likes: 0,
        username: req.body.username
    }

    const newPost = new Post(post);
    newPost.save()
        .then(() => res.json({
            message: {
                type: 'success',
                text: 'Post added'
            }
        }))
        .catch(err => console.log(err));

    // User.findOneAndUpdate({username:req.body.username}, {$push: {posts:post}},(err,user)=>{
    //     if(err || !user) res.json({
    //         error:'error'
    //     })
    //     else{
    //         console.log('ok')
    //     }
    // })
})

router.post('/:id/delete', (req, res) => {
    Post.findByIdAndDelete(req.params.id, (err, post) => {
        if (err || !post) {
            res.json({
                message: {
                    type: 'error',
                    text: 'error, please try again'
                }
            })
        } else {
            LikePost.findOneAndUpdate({ userId: req.body.userId }, { $pull: { posts: { postId: req.params.id } } }, (err, lpost) => {
                if (err || !lpost) {
                    // console.log('err || !lpost', lpost)
                    res.json({
                        message: {
                            type: 'error',
                            text: 'error, please try again'
                        }
                    })
                } else {

                    res.json({
                        message: {
                            type: 'success',
                            text: 'Post deleted'
                        }
                    })
                }
            })

        }
    })


})

router.post('/getall', (req, res) => {

    Post.find({}, (err, posts) => {
        if (err || !posts) {
            res.json({
                error: 'error'
            })
        } else {
            LikePost.find({ userId: req.body.userId }, (err, lposts) => {
                if (err || !lposts) {
                    // console.log('lposts')

                    res.json({
                        posts
                    })
                } else {
                    // console.log(lposts)
                    res.json({
                        posts,
                        likedPosts: lposts[0]
                    })
                }
            })

        }
    })
})

router.post('/getliked', (req, res) => {


    LikePost.find({ userId: req.body.userId }, (err, lposts) => {
        if (err || !lposts) {
            // console.log('lposts')
            console.log('error')
        } else {
            // console.log(lposts[0].posts[0])
            let ids = []
            lposts[0].posts.map(post=>{
                ids.push(post.postId)
            })
            Post.find({_id: {$in:ids}},(err,posts)=>{
                if(err || !posts){
                    console.log('err')
                    res.json({
                        message: 'error'
                    })
                }else{
                    // console.log(posts)
                    res.json({
                        posts
                    })
                }
            })
        }
    })


})

router.post('/:id/like', (req, res) => {
    console.log('like')
    console.log(req.body)
    // const id = req.params.id
    // console.log(req.body.id)
    LikePost.findOneAndUpdate({ userId: req.body.userid }, { userId: req.body.userid, $push: { posts: { postId: req.params.id, body: req.body.post.body, username: req.body.post.username } } }, { upsert: true }, (err, liked) => {
        if (err) {
            res.json({
                message: {
                    type: 'error',
                    text: 'error, please try again'
                }
            })
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'Liked'
                }
            })
        }
    })


})

router.post('/:id/unlike', (req, res) => {
    console.log('unlike')
    // const id = req.params.id
    // console.log(req.body.id)
    LikePost.findOneAndUpdate({ userId: req.body.userid }, { $pull: { posts: { postId: req.params.id } } }, (err, liked) => {
        if (err || !liked) {
            res.json({
                message: {
                    type: 'error',
                    text: 'error, please try again'
                }
            })
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'Unliked'
                }
            })
        }
    })

})

router.post('/:id/addcomment', (req, res) => {
    console.log('addcomment')
    const id = req.params.id
    // console.log(req.body)
    Post.findByIdAndUpdate(id, { $push: { comments: { username: req.body.username, body: req.body.commentbody } } }, (err, post) => {
        // console.log(post)
        if (err || !post) {
            res.json({
                message: {
                    type: 'error',
                    text: 'error, please try again'
                }
            })
        } else {
            console.log('comment added')
            res.json({
                message: {
                    type: 'success',
                    text: 'Comment added'
                }
            })
        }
    })
})

router.post('/:id/deletecomment', (req, res) => {

    Post.findByIdAndUpdate(req.params.id, { $pull: { comments: { _id: req.body.commentId } } }, (err, del) => {
        if (err || !del) {
            res.json({
                message: {
                    type: 'error',
                    text: 'error, please try again'
                }
            })
        } else {
            // console.log('comment added')
            res.json({
                message: {
                    type: 'success',
                    text: 'Deleted'
                }
            })
        }
    })
})


module.exports = router;