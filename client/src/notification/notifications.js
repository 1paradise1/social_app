import { NotificationManager } from 'react-notifications';

export const createNotification = (message) => {
console.log(message)
    switch (message.type) {
        // case 'info':
        //     NotificationManager.info('Info message');
        //     break;
        case 'success':
            NotificationManager.success('Success message', message.text)
            break
        // case 'warning':
        //     NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);
        //     break;
        case 'error':
            NotificationManager.error('Error message', message.text)
            break
    }

}