import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const PrivateRoute = ({ component: Components, ...rest }) => {

    // console.log(Components[0], Components[1])
    return (
        // <Route
        //     {...rest}
        //     render={props =>
        //         localStorage.getItem("token")
        //             ? (
        //                 <Component {...props} />
        //             )
        //             : (
        //                 <Redirect
        //                     // to={{
        //                     //     pathname="/signin",
        //                     //     state: { from: props.location }
        //                     // }}
        //                     to="/signin"
        //                 />
        //             )
        //     }
        // />
        <Route
            {...rest}
            render={props =>
                localStorage.getItem("token")
                    ? (
                        Components.map((Component, i) => {
                            return <Component key={i} {...props} />

                        })
                    )
                    : (
                        <Redirect
                            // to={{
                            //     pathname="/signin",
                            //     state: { from: props.location }
                            // }}
                            to="/signin"
                        />
                    )
            }
        />
    )
}

export default PrivateRoute