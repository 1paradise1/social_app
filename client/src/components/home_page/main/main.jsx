import React from 'react'

import './main.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-regular-svg-icons'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'
import axios from 'axios'
import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { addcomment, addpost, fetchposts, likepost, unlikepost, deletepost, deletecomment, get_posts,  } from '../../../redux/actions/postActions'
import { createNotification } from '../../../notification/notifications'


const Main = () => {
    const dispatch = useDispatch()
    // const user = useSelector(state => state.userReducer.currentUser)

    const username = localStorage.getItem('user')


    const isAdded = useSelector(state => state.postReducer.isAdded)
    const isLiked = useSelector(state => state.postReducer.isLiked)

    const likeunlike = useSelector(state => state.postReducer.likeunlike)
    const commentAdded = useSelector(state => state.postReducer.commentAdded)
    const deletedComment = useSelector(state => state.postReducer.deleteComment)
    const deletedPost = useSelector(state => state.postReducer.deletePost)
    
    // const posts = useSelector(state => state.postReducer.posts)
    // console.log(likeunlike)
    const [addedpost, setPost] = useState('')

    const [posts, setPosts] = useState([])
    const [likedposts, setLikedPosts] = useState({})

    const [comment, setComment] = useState('')


    console.log(useSelector(state => state.postReducer));
    console.log(likeunlike);

    const [classNAme, setClass] = useState('')
    console.log('render')

    useEffect(() => {
       
        getInfo()

    }, [isAdded,likeunlike,commentAdded,deletedPost,deletedComment])

    const getInfo = () => {
        axios.post('/post/getall', { userId: localStorage.getItem('id') })
            .then((res) => {
                setPosts(res.data.posts.reverse())
                if (res.data.likedPosts != null) {
                    setLikedPosts(res.data.likedPosts.posts)
                    dispatch(get_posts(res.data.likedPosts.posts))
                }
            })
    }

    const handleChangePost = e => {
        setPost(e.target.value)
        // console.log(post)
    }
    // console.log(user)

    const submitPost = e => {
        e.preventDefault()


        // axios.post('/post/add', { post, username: user.username })
        //     .then((res) => setPost(''))
        dispatch(addpost(addedpost, username))
        createNotification(isAdded)
        setPost('')
    }
    const handleChangeComment = e => {
        setComment(e.target.value)
        // console.log(post)
    }

    // const submitComment = e => {

    // }
    const handleKeyPress = (e, postId) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            dispatch(addcomment(postId, comment, username))
            setComment('')
            createNotification(commentAdded)
            console.log(commentAdded)
        }
    }

    const setlike = sendedpost => {
        let result = null
        // console.log(sendedpost)
        if (likedposts.length > 0) { result = likedposts.filter(post => post.postId === sendedpost._id) }
        if (result != null && result.length > 0) {
            dispatch(unlikepost(sendedpost._id, localStorage.getItem('id')))
        } else {
            dispatch(likepost(sendedpost, localStorage.getItem('id')))
        }
        // createNotification(likeunlike)
        // getInfo()
        createNotification(likeunlike)

    }

    const deletePost = postId => {
        // console.log(postId)
        dispatch(deletepost(postId))
        createNotification(deletedPost)
    console.log(deletedPost)

    }

    const deleteComment = (commentId, postId) => {
        // dispatch(deletepost(postId))
        dispatch(deletecomment(postId,commentId))
        createNotification(deletedComment)
    }
    const checkLike = (postId) =>{
        let result = null
        if (likedposts.length > 0) { result = likedposts.filter(post => post.postId === postId) }
        if (result != null && result.length > 0) {
            return true
        } else {
            return false
        }
    }

    return (
        <div className="main-container">
            <div className="new-post">
                <div className="post-container">
                    <form onSubmit={submitPost}>
                        <div className="text-area">
                            <div className="user-info">
                                <figure>
                                    <img src={require('../../../images/1.png')} alt="" />
                                </figure>
                                <p className="user-title"><a href="">{username}</a></p>
                            </div>


                            <textarea placeholder="write something..." value={addedpost} onChange={handleChangePost}></textarea>
                            <div className="form-submit">
                                <button type="submit">Post</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
            <div className="posts-list">
                {posts.map((post, i) => {
                    // console.log(post)
                    let liked = checkLike(post._id)
                    return <div key={i} className="post">
                        <div className="post-author">
                            <figure>
                                <img src={require('../../../images/1.png')} alt="" />
                            </figure>
                            <div className="author-info">
                                <div className="author-name">
                                    <a href="#">{post.username ? post.username : 'Stas Diakun'}</a>

                                </div>
                                <span className="post-date">Published: {new Date(post.createdAt).toUTCString()}</span>
                            </div>
                        </div>
                        <div className="post-body">
                            <div className="post-image">
                                <img src={require('../../../images/user-post.jpg')} alt="" />
                            </div>
                            <div className="post-addlike">
                                {/* {chekLike(post._id)} */}
                                {/* <FontAwesomeIcon onClick={() => setlike(post._id)} icon={faHeart} /> */}
                                
                                <FontAwesomeIcon onClick={() => setlike(post)} className={liked ? 'like-icon' : ''} icon={faHeart} />
                                {/* <p className="post-addlike-likes">{post.likes}</p> */}
                                {post.username === username &&
                                    <div className="delete-post">
                                        <button onClick={() => deletePost(post._id)}>Delete</button>
                                    </div>}
                            </div>
                            <div className="post-description">
                                <p className="post-text">
                                    {post.body}
                                </p>
                            </div>

                            <div className="comments">
                                {post.comments.map((comment, i) => {
                                    return <div key={i} className="comment">
                                        <div className="comment-author">
                                            <figure>
                                                <img src={require('../../../images/1.png')} alt="" />
                                            </figure>
                                        </div>

                                        <div className="comment-body">
                                            <div className="comment-head">
                                                {comment.username ? comment.username : 'Jason Borne'}
                                                {comment.username === username &&
                                                <div className="delete-post">
                                                    <button onClick={() => deleteComment(comment._id, post._id)}>Delete</button>
                                                </div>}
                                            </div>
                                            
                                            <p className="comment-text">{comment.body}</p>
                                        </div>
                                    </div>
                                })}
                                <div className="add-comment">
                                    <div className="comment-author">
                                        <figure>
                                            <img src={require('../../../images/1.png')} alt="" />
                                        </figure>
                                    </div>
                                    <form onKeyPress={(e) => handleKeyPress(e, post._id)} className="comment-form">
                                        <textarea value={comment} onChange={handleChangeComment} placeholder="write a comment..." name="" id=""></textarea>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                })}

                
            </div>
        </div>
    )
}

export default Main