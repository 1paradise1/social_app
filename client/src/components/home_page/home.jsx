import React from 'react'
import LikedPosts from './liked_posts/liked'
import Main from './main/main'

import './home.scss'
import { useEffect } from 'react'
import axios from 'axios'
import { useState } from 'react'

const Home = () => {
    // const [user, setUser] = useState({})
    // useEffect(() => {
    //     axios.get(`/user/${localStorage.getItem('id')}`)
    //         .then((res) => setUser(res.data.user))
    // },[])

    return (
        <div className="home-container">
            <Main />
            <LikedPosts />
        </div>
    )
}

export default Home