import { GET_USER, LOGIN_USER, LOGOUT_USER }  from "../types"

const initialState = {
    currentUser: {}
}

const userReducer = (state=initialState, action) =>{
    switch(action.type){
        case LOGIN_USER:
            console.log(action.payload.user)
            return {...state, currentUser: action.payload.user}
        case LOGOUT_USER:
            return{ ...state, currentUser: {}}
        // case GET_USER:
        //     // console.log("lol")
        //     return {...state, currentUser: action.payload}
        default: return state
    }
}

export default userReducer