import { ADD_COMMENT, ADD_POST, DELETE_COMMENT, DELETE_POST, GET_POSTS, LIKE_POST, USER_POSTUNLIKE } from "../types"

const initialState = {
    posts: [],
    isAdded: '',
    isLiked: [],
    likeunlike: '',
    commentAdded: '',
    deletePost: '',
    deleteComment: ''
}

const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSTS:
            return { ...state, isLiked: action.payload }
        case ADD_POST:
            return { ...state, isAdded:  action.payload.message }
        case DELETE_POST:
            return { ...state, deletePost: action.payload.message }
        case LIKE_POST:
            return { ...state, likeunlike: action.payload.message }
        case USER_POSTUNLIKE:
            return { ...state, likeunlike: action.payload.message }
        case ADD_COMMENT:
            return { ...state, commentAdded: action.payload.message }
        case DELETE_COMMENT:
            return { ...state, deleteComment: action.payload.message }
        default: return state
    }
}

export default postReducer