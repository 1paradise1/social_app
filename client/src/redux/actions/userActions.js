import axios from "axios"
import { GET_USER, LOGIN_USER, LOGOUT_USER, REGISTER_USER } from "../types"


// export const getUser = (payload) => {
//     // console.log(payload)
//     return {
//         type: GET_USER,
//         payload
//     }
// }


export const registerUser = (payload) => {
    // console.log(payload)
    return {
        type: REGISTER_USER,
        payload
    }
}


export const loginUser = (userdata) => async dispatch => {
    const response = await axios.post('/user/login', userdata)

    const payload = await response.data

    localStorage.setItem('token', payload.token)
    localStorage.setItem('user', payload.user.username)
    console.log(localStorage)
    // console.log(payload)
    // console.log(localStorage)

    dispatch({
        type: LOGIN_USER,
        payload
    })


}


export const logoutUser = () => async dispatch => {
    localStorage.clear()
    console.log(localStorage.getItem('user'))
    // const response = await axios.post('/user/login', userdata)

    // const payload = await response.data

    // localStorage.setItem('token', payload.token)
    // localStorage.setItem('user', payload.user.username)
    // console.log(localStorage)
    // // console.log(payload)
    // // console.log(localStorage)

    dispatch({
        type: LOGOUT_USER,
    })


}
