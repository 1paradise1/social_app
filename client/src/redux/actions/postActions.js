import axios from "axios"
import { createNotification } from "../../notification/notifications"
import { ADD_POST, GET_POSTS, LIKE_POST, USER_POSTUNLIKE, ADD_COMMENT, DELETE_POST, DELETE_COMMENT } from "../types"

// export const fetchposts = () => async dispatch => {
//     const response = await axios.get('/post/getall')
//     const payload = await response.data

//     dispatch({
//         type: GET_POSTS,
//         payload
//     })

// }

export const get_posts = (payload) =>{
    return {
        type: GET_POSTS,
        payload
    }

}

export const addpost = (post, username) => async dispatch => {
    // console.log(post, username)
    const response = await axios.post('/post/add', { post, username })
    const result = await response.data

    // const payload = post
    
    // if (result.added) {
    dispatch({
        // fetchposts() 
        type: ADD_POST,
        payload: {
            message: result.message
        }
    })
    // } else {

    // }


}

export const deletepost = (postId) => async dispatch => {
    const userId = localStorage.getItem('id')
    // console.log(userId)
    const response = await axios.post(`/post/${postId}/delete`, {userId})
    const result = await response.data

    // const payload = post

    // if (result.added) {
    dispatch({
        // fetchposts() 
        type: DELETE_POST,
        payload: {
            message: result.message
        }
    })
  


}


export const likepost = (post, userid) => async dispatch => {
    // console.log(post)
    const response = await axios.post(`/post/${post._id}/like`, {userid, post})
    const result = await response.data
    // console.log(result)
    // if (result.isOk) {
        // createNotification(result.message)
    dispatch({
        type: LIKE_POST,
        payload: {
            message: result.message
        }
    })
    // } else {

    // }
}

export const unlikepost = (id, userid) => async dispatch => {
    const response = await axios.post(`/post/${id}/unlike`, {userid})
    const result = await response.data
    // console.log(result)
    // if (result.isOk) {
        // createNotification(result.message)

    dispatch({
        type: USER_POSTUNLIKE,
        payload: {
            message: result.message
        }
    })
}

export const addcomment = (id, commentbody, username) => async dispatch => {
    // console.log(id, commentbody, username)
    const response = await axios.post(`/post/${id}/addcomment`, {commentbody,username})
    const result = await response.data
    // console.log(result)
    // if (result.isOk) {
    dispatch({
        type: ADD_COMMENT,
        payload: {
            message: result.message
        }
    })
}

export const deletecomment = (id, commentId) => async dispatch => {
    // console.log(id, commentbody, username)
    const response = await axios.post(`/post/${id}/deletecomment`, {commentId})
    const result = await response.data
    // console.log(result)
    // if (result.isOk) {
    dispatch({
        type: DELETE_COMMENT,
        payload: {
            message: result.message
        }
    })
}
